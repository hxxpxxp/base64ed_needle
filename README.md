## BASE64 needle Generator

Generate REGEX needle that can search inside base64 encoded data

[Try Here](https://hxxpxxp.gitlab.io/base64ed_needle/)
