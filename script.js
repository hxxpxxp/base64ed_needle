let outbox=document.querySelector("#output");
let inbox=document.querySelector("#input");
let ans="";
const textEncoder = new TextEncoder();
document.querySelector("#copy_btn").addEventListener("click",(ev)=>{
	navigator.clipboard.writeText(ans);
});
document.querySelector("#convert_btn").addEventListener("click",(ev)=>{
	ans="";
	let needle=inbox.value;
	let base64Table="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	let encoded=textEncoder.encode(needle);
	let totalBits=(encoded.length)*8;
	for(let i=0;i<3;i++){
		let result="";
		let bitIndex=0;
		let bits=0;
		switch(i){
			case 0:
				bitIndex=0;
				break;
			case 1:
				result+="[";
				for(let j=0;j<4;j++){
					result+=base64Table.charAt((j<<4)|(encoded[0]>>4));
				}
				result+="]";
				bitIndex=4;
				break;
			case 2:
				result+="[";
				for(let j=0;j<16;j++){
					result+=base64Table.charAt((j<<2)|(encoded[0]>>6));
				}
				result+="]";
				bitIndex=2;
				break;
		}
		while(bitIndex<=totalBits-6){
			let byteIndex=Math.floor(bitIndex/8);
			let remainder=bitIndex%8;
			switch(remainder){
				case 0:
					result+=base64Table.charAt(encoded[byteIndex] >> 2);
					break;
				case 6:
					result+=base64Table.charAt(((encoded[byteIndex] & 0b11) << 4) | (encoded[byteIndex+1] >> 4));
					break;
				case 4:
					result+=base64Table.charAt(((encoded[byteIndex] & 0b1111) << 2) | (encoded[byteIndex+1] >> 6));
					break;
				case 2:
					result+=base64Table.charAt(encoded[byteIndex] & 0b111111);
					break;
			}
			bitIndex+=6;
		}
		let byteIndex=Math.floor(bitIndex/8);
		let remainder=bitIndex%8;
		let highbits=0;
		switch(remainder){
			case 6:
				result+="[";
				highbits=(encoded[byteIndex] & 0b11) << 4;
				for(let j=0;j<16;j++){
					result+=base64Table.charAt(highbits | j);
				}
				result+="]";
				break;
			case 4:
				result+="[";
				highbits=(encoded[byteIndex] & 0b1111) << 2;
				for(let j=0;j<4;j++){
					result+=base64Table.charAt(highbits | j);
				}
				result+="]";
				break;
		}
		i==0?ans+=result:ans+="|"+result;
	}
	ans=ans.replaceAll("/","\\/");
	ans=ans.replaceAll("+","\\+");
	outbox.textContent=ans;

});
